# Copyright 2013, 2014 Michael Forney
# Distributed under the terms of the GNU General Public License v2

require github [ user=michaelforney ]

SUMMARY="WLD is a primitive drawing library targeted at Wayland"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    drm
    wayland
    video_drivers:
        (
            intel
            nouveau
        ) [[ requires = [ drm ] ]]
"

DEPENDENCIES="
    build+run:
        media-libs/fontconfig
        media-libs/freetype:2
        x11-libs/pixman

        drm? ( x11-dri/libdrm[video_drivers:intel?][video_drivers:nouveau?] )
        wayland? ( sys-libs/wayland )
"

BUGS_TO="mforney@mforney.org"

DEFAULT_SRC_COMPILE_PARAMS=( V=1 )

src_configure() {
    local drm_drivers=( ) wayland_interfaces=( )

    option video_drivers:intel      && drm_drivers+=( intel )
    option video_drivers:nouveau    && drm_drivers+=( nouveau )
    option drm && option wayland    && wayland_interfaces+=( drm )
    option wayland                  && wayland_interfaces+=( shm )

    cat > config.mk <<EOF
PREFIX              = /usr/$(exhost --target)
LIBDIR              = /usr/$(exhost --target)/lib

ENABLE_STATIC       = 1
ENABLE_SHARED       = 1
ENABLE_PIXMAN       = 1
ENABLE_DRM          = $(option drm 1 0)
ENABLE_WAYLAND      = $(option wayland 1 0)
DRM_DRIVERS         = ${drm_drivers[*]}
WAYLAND_INTERFACES  = ${wayland_interfaces[*]}
EOF

    cat config.mk
}

