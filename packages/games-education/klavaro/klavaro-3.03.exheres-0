# Copyright 2009 Michael Forney
# Distributed under the terms of the GNU General Public License v2

require sourceforge \
    gtk-icon-cache \
    autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

SUMMARY="Klavaro is just another free touch typing tutor program"

BUGS_TO="mforney@mforney.org"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
LANGUAGES=( ar bg bn ca cs da de el en_GB eo es eu fi fr gl hr hu it kk ky nb nl pl pt_BR ru sr sv
            te uk ur vi wo zh_CN )
MYOPTIONS=""
#    ( linguas: ar bg bn ca cs da de el en_GB eo es eu fi fr gl hr hu it kk ky nb nl pl pt_BR ru sr sv
#               te uk ur vi wo zh_CN )


DEPENDENCIES="
    build:
        dev-util/intltool[>=0.50]
        sys-devel/gettext[>=0.18.3]
        virtual/pkg-config
    build+run:
        net-misc/curl[>=2.18.12]
        x11-libs/gtk+:3[>=3.8.0]
        x11-libs/cairo[>=1.4.0]
        x11-libs/pango[>=1.16.0]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-nls
    --disable-static
)

DEFAULT_SRC_INSTALL_PARAMS=(
    desktopdir=/usr/share/applications
    appdatadir=/usr/share/appdata
)

src_prepare() {
    # TODO: report upstream, respect localedir
    edo sed \
        -e 's:$(prefix)/$(DATADIRNAME)/locale:$(datadir)/locale:g' \
        -i src/Makefile.am

    # drop if po/Makefile.in.in doesn't have 'itlocaledir = $(prefix)/$(DATADIRNAME)/locale' anymore
    edo intltoolize --force --automake

    autotools_src_prepare
}

src_install() {
    default

    # workaround since locales aren't being installed
    for i in "${LANGUAGES[@]}"; do
        insinto /usr/share/locale/${i}/LC_MESSAGES
        newins po/${i}.gmo ${PN}.mo
    done
}

